#!/usr/bin/python
# -*- coding: utf-8 -*-
#################
# Script to extract snow keff data from automatic measurements of 
# thermal conductivity needle probes from Hukseflux (TP08 or TP02)
# Author: Mathieu Barrere, 2015
# For more information, see: Domine, F., M. Barrere, D. Sarrazin, S. Morin, and L. Arnaud (2015),
# Automatic monitoring of the effective thermal conductivity of snow in a low-Arctic shrub tundra, The Cryosphere, 9(3), 1265-1276.
# https://doi.org/10.5194/tc-9-1265-2015
#################


### Import python modules
import os, csv
from matplotlib.pyplot import *
import matplotlib.dates as mdates
import numpy as np
import math
import datetime as dt
import time
from scipy import stats

### Plot parameters
matplotlib.rcParams['xtick.labelsize'] = 26
matplotlib.rcParams['ytick.labelsize'] = 26
matplotlib.rcParams['axes.labelsize'] = 26
matplotlib.rcParams['axes.titlesize'] = 26
matplotlib.rcParams['legend.fontsize'] = 26
matplotlib.rcParams['figure.figsize'] = 16., 12.


site = raw_input("Enter the site name: ")

### Open measurement data
# open file
mes_cond  = open('keff_data/'+site+'/filename.dat', 'rb')
reader = csv.reader(mes_cond, delimiter=',')
# heating time, in seconds
period = 100
# probe heights linked to the beginning time of the measurement, in minutes
dico = {'02':'44cm','10':'34cm','17':'24cm','25':'14cm'}


### Parameters
# convection thresholds
if period == 100:       #heating time = 100s
	DTlim1 = 1.07 	# no convection
	DTlim2 = 1.18   # convection
	beg1 = 30	#interval 1 (no convection)
	end1 = 90	#interval 1 (no convection)
else:                   #heating time = 150s
	DTlim1 = 1.21 	# no convection
	DTlim2 = 1.27   # convection
	beg1 = 40	#interval 1 (no convection)
	end1 = 100	#interval 1 (no convection)
beg2 = 20		#interval 2 (convection)
end2 = 50		#interval 2 (convection)

### Initialisation of variables
t = [] 		#time
ts = []		#measurement time
DT = []		#heating amount
q = []		#heating power
Tneige = []	#snow temperature
kh = []		#keff from heating
kc = []		#keff from cooling
rh = []		#R² heating
rc = []		#R² cooling
rownum = 0
i = 0

### Creating the working directory
rundir = 'keff_data/'+site+'/'
run = str(beg1)+'-'+str(end1)+'_'+str(beg2)+'-'+str(end2)+'/'
if not os.path.exists(rundir+run):
	os.makedirs(rundir+run)

### Program outputs
savecurves = raw_input("Do you want to save each measurement plot? (y/n)")
savekh = raw_input("Do you want to save the time serie plot of keff_heat? (y/n)")
savekc = raw_input("Do you want to save the time serie plot of keff_cool? (y/n)")
savedat = raw_input("Do you want to save the keff values in a data file? (y/n)")
savetemp = raw_input("Do you want to save the time serie plot of snow temperature? (y/n)")


### Function to extract heating and cooling curves
def cycle(t,DT,q,date,T_Pt1000):
	date_date = dt.datetime.strptime(date,'%Y-%m-%d %H:%M:%S')
	# print dt.datetime.strftime(date_date,'%M')  #show the time of measurement in minutes
	DT = np.array(DT)
	q = np.mean(q[0:period-1])	#q mean over the heating cycle, to obtain cooling curves 
	# print q
	warn1 = str("_WARNING : risk of melting")
	warn2 = str("_WARNING : convection")
	warn3 = str("_WARNING : risk of convection")

	# plot measurement cycle
	ax1.plot(t,DT,label=dico[dt.datetime.strftime(date_date,'%M')])
	ax1.set_xlabel('t')
	ax1.set_ylabel('$\Delta$T')
	ax1.legend(loc='upper right')

	#title and warnings
	#snow temperature closed to melting
	if T_Pt1000 >= -2.5:
		ax1.set_title(date + warn1 + str("\nTsnow=") + str(T_Pt1000) + str(" DTmax=") + str(max(DT)-min(DT)))
		war = 1
		beg = beg1
		end = end1
	#convection occurs
	elif max(DT)-min(DT) >= DTlim2:
		ax1.set_title(date + warn2 + str("\nTsnow=") + str(T_Pt1000) + str(" DTmax=") + str(max(DT)-min(DT)))
		war = 2
		beg = beg2
		end = end2
	#risk of convection
	elif max(DT)-min(DT)>=DTlim1 and max(DT)-min(DT)<DTlim2:
		ax1.set_title(date + warn3 + str("\nTsnow=") + str(T_Pt1000) + str(" DTmax=") + str(max(DT)-min(DT)))
		war = 3
		beg = beg1
		end = end1
	#no convection, no melting
	else:
		ax1.set_title(date + str("\nTsnow=") + str(T_Pt1000) + str("  DTmax=") + str(max(DT)-min(DT)))
		war = 0
		beg = beg1
		end = end1

	# draw gray areas to show the interval of regression
	if war != 3:
		limits = ax1.get_ylim()
		ax1.fill_between((beg,end),limits[0],limits[1],color='0.75',alpha=0.5)
		ax1.fill_between((period+beg,period+end),limits[0],limits[1],color='0.75',alpha=0.5)

	return (4*math.pi*DT)/q,war,beg,end


### Function to calculate keff
def cond(stage,t,normT,beg,end,war):
	t = np.array(t)
	# heating curve
	if stage == 'heating':
		lnt = np.log(t)
		str = 'ln(t)'

		### Linear regression
		# first test if convection occurs for DTmax between thresholds
		# if keff extracted from the higher interval is > than keff
		# from the lower interval by more than 5%, convection occurs
		if war == 3:
			limits = ax1.get_ylim()
			p_1 = np.polyfit(lnt[beg1:end1],normT[beg1:end1],1)
			p_2 = np.polyfit(lnt[beg2:end2],normT[beg2:end2],1)
			keff_1 = 1/p_1[0]
			keff_2 = 1/p_2[0]
			if ( keff_1 <= (keff_2 + 0.05*keff_2) ):#no convection			
				beg = beg1
				end = end1
				keff = keff_1
				reg = p_1[0]*lnt[beg:end] + p_1[1]
				ax1.fill_between((beg,end),limits[0],limits[1],color='0.75',alpha=0.5)
				ax1.fill_between((period+beg,period+end),limits[0],limits[1],color='0.75',alpha=0.5)
			else: 				    	#convection
				beg = beg2
				end = end2
				keff = keff_2
				reg = p_2[0]*lnt[beg:end] + p_2[1]
				ax1.fill_between((beg,end),limits[0],limits[1],color='0.75',alpha=0.5)
				ax1.fill_between((period+beg,period+end),limits[0],limits[1],color='0.75',alpha=0.5)
		else:
			p = np.polyfit(lnt[beg:end],normT[beg:end],1)
			reg = p[0]*lnt[beg:end] + p[1]
			keff = 1/p[0]

		# coefficient of determination
		slope, intercept, r_value, p_value, std_err = stats.linregress(lnt[beg:end],normT[beg:end])
		r2 = r_value**2

		# plot heating curve
		ax2.plot(lnt,normT,'o')
		ax2.plot(lnt[beg:end],reg,'ro',label='k$_{eff}$=%.3f' % (keff)+'\nR$^{2}$=%.3f' % (r2))
		ax2.legend(loc='lower right')
		ax2.set_xlabel(str)
		ax2.set_ylabel('4$\pi\Delta$T/q')
		
	# cooling curve
	else:
		lnt = np.log(t) - np.log(t-period)
		str = 'ln(t) - ln(t-%d' % period+')'

		if war == 3:
			p_1 = np.polyfit(lnt[beg1:end1],normT[beg1:end1],1)
			p_2 = np.polyfit(lnt[beg2:end2],normT[beg2:end2],1)
			keff_1 = 1/p_1[0]
			keff_2 = 1/p_2[0]
			if ( keff_1 <= (keff_2 + 0.05*keff_2) ):#no convection	
				beg = beg1
				end = end1
				keff = keff_1
				reg = p_1[0]*lnt[beg:end] + p_1[1]
			else: 					#convection
				beg = beg2
				end = end2
				keff = keff_2
				reg = p_2[0]*lnt[beg:end] + p_2[1]
		else:
			p = np.polyfit(lnt[beg:end],normT[beg:end],1)
			reg = p[0]*lnt[beg:end] + p[1]
			keff = 1/p[0]

		slope,intercept,r_value,p_value,std_err = stats.linregress(lnt[beg:end],normT[beg:end])
		r2 = r_value**2

		ax3.plot(lnt,normT,'o')
		ax3.plot(lnt[beg:end],reg,'ro',label='k$_{eff}$=%.3f' % (keff)+'\nR$^{2}$=%.3f' % (r2))
		ax3.legend(loc='upper left')
		ax3.set_xlabel(str)

	return keff,r2



### keff automatic extraction program
for row in reader:
	ax1 = subplot(211)
	ax2 = subplot(223)
	ax3 = subplot(224)

	if rownum > 3: 	#headers
		#initialisation period
		if i < period+2:
			i += 1
		#measurement cycle
		elif i < period*3:
			#start heating
			if i == period+2:
				Tneige.append(float(row[6]))
				T_Pt1000 = float(row[6])
				ts.append(str(row[0]))
				date = str(row[0]) #time of beg cycle
			#measurement cycle
			t.append(float(row[2]))
			DT.append(float(row[7]))
			q.append(float(row[-1]))
			i += 1
		#end of cycle
		elif i == period*3:
			t.append(float(row[2]))
			DT.append(float(row[7]))
			q.append(float(row[-1]))
			
			#extract curves
			normT,war,beg,end = cycle(t,DT,q,date,T_Pt1000)

			#heating period
			stage = 'heating'
			(keff_heat,R2_heat) = cond(stage,t[0:period-1],normT[0:period-1],beg,end,war)

			#cooling period
			stage = 'cooling'
			(keff_cool,R2_cool) = cond(stage,t[period:],normT[period:],beg,end,war)

			#keff value is kept depending on R²
			if R2_heat < 0.970: #bad fit, keff is not kept
				keff_heat = np.nan
				# print R2_heat, date
			kh.append(float(keff_heat))
			rh.append(float(R2_heat))

			if R2_cool < 0.970:
				keff_cool = np.nan
			kc.append(float(keff_cool))
			rc.append(float(R2_cool))

			# save curve plots
			if savecurves == 'y':
				if war == 0:
					savefig(rundir+run+site+'-keff_'+date+'.png',bbox_inches = "tight")
				elif war == 1:
					savefig(rundir+run+site+'-keff_'+date+'_WARMELT.png',bbox_inches = "tight")
				elif war == 3:
					savefig(rundir+run+site+'-keff_'+date+'_WARRISK.png',bbox_inches = "tight")
				else:
					savefig(rundir+run+site+'-keff_'+date+'_WARCONV.png',bbox_inches = "tight")
			clf()
			i += 1

		# new cycle, reset variables
		else:
			t = []
			DT = []
			q = []
			i = 1

	rownum += 1

close()


### Convert time strings in dates
date = []
for t in ts:
	date.append(dt.datetime.strptime(t,'%Y-%m-%d %H:%M:%S'))
date_jour = [dt.datetime.strftime(d,'%Y-%m-%d') for d in date]

### Retrieve the height of each probe
capt = [dt.datetime.strftime(d,'%M') for d in date]
capt2 = sorted(set(capt))
capt_height = [dico[c] for c in capt]

date = np.array(date)
date_jour = np.array(date_jour)
Tneige = np.array(Tneige)
capt = np.array(capt)
kh = np.array(kh)
kc = np.array(kc)


### Save keff evolution plots
# keff_heat time serie
if savekh == 'y':
	if len(capt) > len(kh):
		capt = np.resize(capt,len(kh))

	colors = ['r','b','g','m','c','y']
	forms = ['-o','-^','-s','-D','-*','-p']
	num = 0

	for c in capt2:
		col = colors[num]
		form = forms[num]
		plot(date[capt==c], kh[capt==c], form, color=col, lw='2', label=dico[c])
		num += 1	
	
	gca().xaxis.set_major_formatter(mdates.DateFormatter('%d %b. %y'))
	gcf().autofmt_xdate()
	ylabel('k$_{eff}$ (W m$^{-1}$ K$^{-1}$)')
	legend(loc='best')
	savefig(rundir+site+'-keff-heat.png',bbox_inches = "tight")
	close()

# keff_cool time serie
if savekc == 'y':
	if len(capt) > len(kc):
		capt = np.resize(capt,len(kc))

	colors = ['r','b','g','m','c','y']
	forms = ['-o','-^','-s','-D','-*','-p']
	num = 0
	
	for c in capt2:
		col = colors[num]
		form = forms[num]
		plot(date[capt==c], kc[capt==c], form, color=col, lw='2', label=dico[c])
		num += 1
		
	gca().xaxis.set_major_formatter(mdates.DateFormatter('%d %b. %y'))
	gcf().autofmt_xdate()
	ylabel('k$_{eff}$ (W m$^{-1}$ K$^{-1}$)')
	legend(loc='best')
	savefig(rundir+site+'-keff-cool.png',bbox_inches = "tight")
	close()


### Save data file
if savedat == 'y':

	# #1 file with all probes
	# fileout = open(rundir+run+site+'-keff.dat',"wb")
	# table = [date_jour,kh,capt_height]
	# tableout = zip(*table)
	# results = csv.writer(fileout,delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
	# for row in tableout:
	# 	results.writerow(row)
	# fileout.close()

	#1 file for each probe
	for c in capt2:
		table = [date_jour[capt==c], kh[capt==c]]
		tableout = zip(*table)
		fileout = open(rundir+site+'-keff'+dico[c]+'.dat',"wb")
		results = csv.writer(fileout,delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
		for row in tableout:
			results.writerow(row)
		fileout.close()


### Save snow temperature evolution plot
if savetemp == 'y':
	colors = ['r','b','g','m','c','y']
	forms = ['-o','-^','-s','-D','-*','-p']
	num = 0
	for c in capt2:
		col = colors[num]
		form = forms[num]
		plot(date[capt==c],Tneige[capt==c], form, color=col, lw='2', label=dico[c])
		num += 1
	gca().xaxis.set_major_formatter(mdates.DateFormatter('%d %b. %y'))
	gcf().autofmt_xdate()
	axhline(y=0,ls='-.',color='k')
	ylabel('Snow temperature ($^\circ$C)')
	legend(loc='best')
	savefig(rundir+site+'-T_Pt1000.png',bbox_inches = "tight")
	close()
